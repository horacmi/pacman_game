# PacmanGame

Simple Pacman clone using Gosu library

## Requirements

Built using ruby 2.2

## Installation

``
gem install pacman_game
``

## Usage

Move with arrow keys
ESC to pause

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
