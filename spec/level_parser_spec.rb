require 'spec_helper'
require_relative '../lib/pacman_game/level_parser'

describe PacmanGame::LevelParser do
  
  subject { PacmanGame::LevelParser }

  it 'should fail in case of multiple pacman starts' do
    file = "#{File.dirname(__FILE__)}/levels/multiple_pacman.txt"
    expect{ subject.load_level(file) }.to raise_error RuntimeError
  end

  it 'should fail in case of invalid tile' do 
    file = "#{File.dirname(__FILE__)}/levels/invalid_level.txt"
    expect{ subject.load_level(file) }.to raise_error RuntimeError
  end

  it 'should fail in case of too many ghosts' do
    file = "#{File.dirname(__FILE__)}/levels/too_many_ghosts.txt"
    expect{ subject.load_level(file) }.to raise_error RuntimeError
  end

  context 'Correct level' do
    let (:file) do
      "#{File.dirname(__FILE__)}/levels/correct.txt"
    end

    it 'should return Level object' do
      expect(subject.load_level(file).class).to eq PacmanGame::Level
    end

    it 'should get correct level dimensions' do
      level = subject.load_level(file)
      expect(level.width).to eq 5
      expect(level.height).to eq 3
      expect(subject.load_level(file).class).to eq PacmanGame::Level
    end

    it 'should get correct pacman starting location' do
      level = subject.load_level(file)
      expect(level.pacman_start).to eq [2, 1]
    end

    it 'should get correct ghost starting locations' do
      level = subject.load_level(file)
      expect(level.ghosts_start).to eq [[1, 1], [3, 1]]
    end
  end
end