require_relative 'character'
require 'gosu'

module PacmanGame
  # Pacman player class
  class Pacman < Character
    ANIMATION_LENGTH = 3
    ANIMATION_RATE = 0.1

    def initialize(level)
      super(level, level.pacman_start)
      @frame = 0
      @frame_update = Time.now
    end

    def update
      animate
      check_input
      move
    end

    def draw
      @sprites[@frame].draw_rot(@x, @y, 0, @direction)
    end

    protected

    def animate
      now = Time.now
      return unless now - @frame_update > ANIMATION_RATE
      @frame = (@frame + 1) % ANIMATION_LENGTH
      @frame_update = now
    end

    def check_input
      if Window.instance.button_down?(Gosu::KbRight)
        @direction = Direction::RIGHT
      elsif Window.instance.button_down?(Gosu::KbLeft)
        @direction = Direction::LEFT
      elsif Window.instance.button_down?(Gosu::KbUp)
        @direction = Direction::UP
      elsif Window.instance.button_down?(Gosu::KbDown)
        @direction = Direction::DOWN
      end
    end

    def can_go_right?
      @level.passable?(tile_pos(@x) + 1,
                       tile_pos(@y + SPRITE_SIZE / 2))
    end

    def can_go_left?
      @level.passable?(tile_pos(@x + SPRITE_SIZE) - 1,
                       tile_pos(@y + SPRITE_SIZE / 2))
    end

    def can_go_up?
      @level.passable?(tile_pos(@x + SPRITE_SIZE / 2),
                       tile_pos(@y + SPRITE_SIZE) - 1)
    end

    def can_go_down?
      @level.passable?(tile_pos(@x + SPRITE_SIZE / 2),
                       tile_pos(@y) + 1)
    end

    def move
      case @direction
      when Direction::RIGHT
        @x += @speed if can_go_right?
      when Direction::LEFT
        @x -= @speed if can_go_left?
      when Direction::UP
        @y -= @speed if can_go_up?
      when Direction::DOWN
        @y += @speed if can_go_down?
      end
    end
  end
end
