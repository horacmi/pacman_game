module PacmanGame
  # Class representing score board shown during game
  class Scoreboard
    attr_accessor :score, :lives, :food_count
    def initialize(food_count)
      @score = 0
      @lives = 3
      @food_count = food_count
    end

    def draw
      draw_score
      draw_lives
    end

    protected

    def draw_score
      score_text = Gosu::Image.from_text(Window.instance,
                                         "Score: #{@score}",
                                         Gosu.default_font_name,
                                         30)
      score_text.draw(750, 100, 0)
    end

    def draw_lives
      lives_text = Gosu::Image.from_text(Window.instance,
                                         "Lives: #{@lives}",
                                         Gosu.default_font_name,
                                         30)
      lives_text.draw(750, 300, 0)
    end
  end
end
