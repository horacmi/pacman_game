module PacmanGame
  # Class representing single game character
  class Character
    SPRITE_SIZE = 24
    SPEED = 5

    def initialize(level, start)
      @level = level
      @start_pos = start
      @x = pixel_pos(start[0])
      @y = pixel_pos(start[1])
      @speed = SPEED
      @direction = Direction::STOP
      @sprites = Window.instance.character_sprites
    end

    def reset
      @x = pixel_pos(@start_pos[0])
      @y = pixel_pos(@start_pos[1])
      @direction = Direction::STOP
    end

    def pos
      [(@x + SPRITE_SIZE / 2) / Window::TILE_SIZE,
       (@y + SPRITE_SIZE / 2) / Window::TILE_SIZE]
    end

    protected

    def tile_pos(pixel_pos)
      pixel_pos / Window::TILE_SIZE
    end

    def pixel_pos(tile_pos)
      tile_pos * Window::TILE_SIZE
    end
  end
end
