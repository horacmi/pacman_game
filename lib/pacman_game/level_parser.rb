require_relative 'level'
require_relative 'tile'

module PacmanGame
  # Handles parsing level information from input file into Level class structure
  class LevelParser
    # Possible tokens within input level file
    module Token
      WALL = '#'.freeze
      EMPTY = ' '.freeze
      STAR = '*'.freeze
      PACMAN = 'P'.freeze
      GHOST = 'G'.freeze
    end

    class << self
      def load_level(level_file)
        level = Level.new(level_file)
        File.open(level_file).each_with_index do |line, line_number|
          level[line_number] = parse_line(line, line_number, level)
        end
        level
      end

      def save_level(level_file)
      end

      def new_level(width, height)
        level = Level.new
        height.times do |x|
          row = []
          width.times do |y|
            tile = Tile.new(TILE::EMPTY, y, x)
            row << tile
          end
          level[x] << row
        end
        level
      end

      protected

      def parse_line(line, line_number, level)
        level_row = []
        line.strip.split('').each_with_index do |token, idx|
          level_row << parse_token(token, line_number, idx, level)
        end
        level_row
      end

      def parse_token(token, row, col, level)
        case token
        when Token::WALL
          return Tile.new(Tile::WALL, col, row)
        when Token::EMPTY
          return Tile.new(Tile::PELLET, col, row)
        when Token::STAR
          return Tile.new(Tile::STAR, col, row)
        when Token::PACMAN
          raise 'Multiple pacman start positions' unless level.pacman_start.nil?
          level.pacman_start = [col, row]
          return Tile.new(Tile::EMPTY, col, row)
        when Token::GHOST
          raise 'Too many ghosts (max is 4)' if level.ghosts_start.size == 4
          level.ghosts_start << [col, row]
          return Tile.new(Tile::PELLET, col, row)
        else
          raise 'Invalid tile type'
        end
      end
    end
  end
end
