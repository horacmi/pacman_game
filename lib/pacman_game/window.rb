require 'gosu'
require 'singleton'

module PacmanGame
  # Main window class
  class Window < Gosu::Window
    include Singleton

    def self.sprite_path(file)
      base_dir = File.expand_path('../../..', __FILE__)
      File.join(base_dir, 'media', 'images', file)
    end

    WIDTH = 1024
    HEIGHT = 768
    TILE_SIZE = 24
    CHARACTER_SPRITE_FILE = sprite_path('characters.png')
    BACKGROUND_SPRITE_FILE = sprite_path('background.png')

    attr_reader :character_sprites, :tile_sprites
    attr_reader :width, :height
    attr_accessor :state

    def initialize(width = WIDTH, height = HEIGHT, fullscreen = false)
      super
      @width = width
      @height = height
      self.caption = 'Pacman game'
      @character_sprites = Gosu::Image.load_tiles(self,
                                                  CHARACTER_SPRITE_FILE,
                                                  TILE_SIZE, TILE_SIZE, false)
      @tile_sprites = Gosu::Image.load_tiles(self,
                                             BACKGROUND_SPRITE_FILE,
                                             TILE_SIZE, TILE_SIZE, false)
      @state = nil
    end

    def load_state(state)
      @state = state
    end

    def needs_cursor?
      @mouse
    end

    def update
      @state.update
    end

    def draw
      @state.draw
    end

    def button_down(id)
      @state.button_down(id)
    end
  end
end
