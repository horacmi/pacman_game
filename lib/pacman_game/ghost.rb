module PacmanGame
  # Class representing a ghost character
  class Ghost < Character
    # Enum of possible ghost types
    module Type
      INKY = 0
      BINKY = 1
      PINKY = 2
      CLYDE = 3
      SCARED = 4
    end

    SCARED_TIMER = 5
    SCATTER_TIMER = 7
    CHASE_TIMER = 20
    SPRITE_OFFSET = 3
    SCARED_SPEED = 3

    def initialize(game, type)
      super(game.level, game.level.ghosts_start[type])
      @game = game
      @type = type
      @home = @level.corners[@type]
      scatter
      @next_tile = best_move
      turn
    end

    def update
      case @mode
      when :scatter
        scatter_state
      when :chase
        chase_state
      when :scared
        scared_state
      end
    end

    def draw
      @sprites[SPRITE_OFFSET + @type].draw_rot(@x, @y, 0, 0.0)
    end

    def scare
      @mode = :scared
      @last_type = @type
      @type = Type::SCARED
      @speed = SCARED_SPEED
      @timer = Time.now
    end

    def scared?
      @mode == :scared
    end

    def reset
      @x = pixel_pos(@start_pos[0])
      @y = pixel_pos(@start_pos[1])
      @direction = Direction::STOP
      @next_tile = best_move
      turn
    end

    protected

    def best_move
      moves = @level.possible_moves(pos)
      moves.delete(opposite_direction) # Ghost is never allowed to turn around
      best = pick_best_move(moves)
      best[1]
    end

    def pick_best_move(moves)
      moves.min_by do |_dir, tile|
        manhattan_distance(tile, @target)
      end
    end

    def opposite_direction
      return Direction::RIGHT if @direction == Direction::LEFT
      return Direction::LEFT if @direction == Direction::RIGHT
      return Direction::UP if @direction == Direction::DOWN
      return Direction::DOWN if @direction == Direction::UP
    end

    def manhattan_distance(tile1, tile2)
      (tile1[0] - tile2[0]).abs + (tile1[1] - tile2[1]).abs
    end

    def reached_destination?
      pos == @next_tile
    end

    def turn
      if @next_tile[0] > pos[0]
        @direction = Direction::RIGHT
      elsif @next_tile[0] < pos[0]
        @direction = Direction::LEFT
      elsif @next_tile[1] > pos[1]
        @direction = Direction::DOWN
      elsif @next_tile[1] < pos[1]
        @direction = Direction::UP
      end
    end

    def move
      case @direction
      when Direction::RIGHT
        @x += @speed
      when Direction::LEFT
        @x -= @speed
      when Direction::UP
        @y -= @speed
      when Direction::DOWN
        @y += @speed
      end
    end

    def state_update
      if reached_destination?
        @next_tile = best_move
        turn
      end
      move
    end

    def scatter_state
      state_update
      now = Time.now
      return if now - @timer < SCATTER_TIMER
      chase
    end

    def chase_state
      @target = @game.player.pos
      state_update
      now = Time.now
      return if now - @timer < CHASE_TIMER
      scatter
    end

    def scared_state
      @target = @level.random_tile
      state_update
      now = Time.now
      return if now - @timer < SCARED_TIMER
      @type = @last_type
      @speed = SPEED
      chase
    end

    def scatter
      @mode = :scatter
      @target = @home
      @timer = Time.now
    end

    def chase
      @mode = :chase
      @timer = Time.now
    end
  end
end
