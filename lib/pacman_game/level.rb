module PacmanGame
  # Parsed information about current level
  class Level
    attr_accessor :pacman_start, :ghosts_start
    attr_reader :input_file

    def initialize(file)
      @grid = []
      @pacman_start = nil
      @ghosts_start = []
      @input_file = file
    end

    def width
      @grid[0].size
    end

    def height
      @grid.size
    end

    # Returns number of tiles containing either pellet or star
    def food_count
      count = 0
      @grid.flatten.each do |tile|
        count += 1 if tile.contains_pellet? || tile.contains_star?
      end
      count
    end

    def [](*args)
      return @grid[args[0]] if args.size == 1
      @grid[args[0]][args[1]] if args.size > 1
    end

    def []=(*args)
      @grid[args[0]] = args[1] if args.size == 2
      @grid[args[0]][args[1]] = args[2] if args.size > 2
    end

    # Returns true if character can pass through tile at position (x, y)
    def passable?(x, y)
      within_bounds?(x, y) && @grid[y][x].passable?
    end

    # Returns true if tile at position (x, y) contains a pellet piece
    def contains_pellet?(x, y)
      within_bounds?(x, y) && @grid[y][x].contains_pellet?
    end

    # Returns true if tile at position (x, y) contains a star piece
    def contains_star?(x, y)
      within_bounds?(x, y) && @grid[y][x].contains_star?
    end

    # Returns [x, y] coordinate of random tile within level bounds
    def random_tile
      x = Random.rand(width)
      y = Random.rand(height)
      [x, y]
    end

    # Returns coordinates of level corner tiles
    def corners
      top_left = [0, 0]
      top_right = [0, width - 1]
      bottom_left = [height - 1, 0]
      bottom_right = [height - 1, width - 1]
      [top_left, top_right, bottom_left, bottom_right]
    end

    # Returns hash of possible moves from given tile
    # Hash item keys are move directions, values are target tile coordinations
    def possible_moves(tile)
      x, y = tile
      moves = {}
      moves[Direction::RIGHT] = [x + 1, y] if passable?(x + 1, y)
      moves[Direction::LEFT] = [x - 1, y] if passable?(x - 1, y)
      moves[Direction::DOWN] = [x, y + 1] if passable?(x, y + 1)
      moves[Direction::UP] = [x, y - 1] if passable?(x, y - 1)
      moves
    end

    def draw
      @grid.each do |row|
        row.each(&:draw)
      end
    end

    def eat(x, y)
      @grid[y][x].eat
    end

    def to_s
      @grid.each do |row|
        row.each do |tile|
          print(tile.to_s)
        end
        print("\n")
      end
    end

    protected

    # Checks if (x, y) coordinates are within level bounds
    def within_bounds?(x, y)
      x >= 0 && y >= 0 && x < width && y < height
    end
  end
end
