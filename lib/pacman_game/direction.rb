module PacmanGame
  # Enum class of possible move directions
  class Direction
    RIGHT = 0.0
    DOWN = 90.0
    LEFT = 180.0
    UP = 270.0
    STOP = 360.0
  end
end
