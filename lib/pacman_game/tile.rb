require 'gosu'

module PacmanGame
  # Class representing single tile in a game level
  class Tile
    # Different types of tiles
    EMPTY = 0
    PELLET = 1
    STAR = 2
    WALL = 3

    def initialize(type, x, y)
      @type = type
      @x = x
      @y = y
      @sprites = Window.instance.tile_sprites
      @redraw = true
    end

    def passable?
      @type != WALL
    end

    def contains_pellet?
      @type == PELLET
    end

    def contains_star?
      @type == STAR
    end

    def needs_redraw?
      @redraw
    end

    def draw
      @sprites[@type].draw_rot(@x * Window::TILE_SIZE,
                               @y * Window::TILE_SIZE,
                               0, 0.0)
      @redraw = false
    end

    def eat
      @type = EMPTY
      @redraw = true
    end

    def to_s
      @type
    end
  end
end
