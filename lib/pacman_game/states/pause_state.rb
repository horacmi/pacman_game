require_relative 'game_state'

require 'singleton'

module PacmanGame
  # State handling pause screen during game
  class PauseState < GameState
    include Singleton

    attr_accessor :play_state

    def initialize
      @message = Gosu::Image.from_text(Window.instance,
                                       'Game paused',
                                       Gosu.default_font_name,
                                       70)
      @info = Gosu::Image.from_text(Window.instance,
                                    "SPACE - Continue\nESCAPE - Quit",
                                    Gosu.default_font_name,
                                    25)
      @play_state = nil
    end

    def draw
      @message.draw(width_text_offset(@message),
                    height_text_offset(@message),
                    0)
      @info.draw(width_text_offset(@info),
                 height_text_offset(@info) + 100,
                 1)
    end

    def button_down(id)
      Window.instance.close if id == Gosu::KbEscape
      GameState.switch(@play_state) if id == Gosu::KbSpace && @play_state
    end
  end
end
