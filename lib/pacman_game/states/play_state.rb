require 'gosu'

require_relative '../pacman'
require_relative '../ghost'
require_relative '../scoreboard'
require_relative 'game_state'
require_relative 'over_state'
require_relative 'pause_state'

module PacmanGame
  # State representing a single game of pacman
  class PlayState < GameState
    PELLET_SCORE = 100
    STAR_SCORE = 250
    GHOST_SCORE = 500

    attr_accessor :score, :lives
    attr_reader :level, :player

    def initialize(level)
      @level = level
      @player = Pacman.new(@level)
      load_ghosts
      @scoreboard = Scoreboard.new(@level.food_count)
    end

    def update
      check_game_over
      @player.update
      check_food_collision
      @ghosts.each(&:update)
      check_ghost_collision
    end

    def draw
      @level.draw
      @player.draw
      @ghosts.each(&:draw)
      @scoreboard.draw
    end

    def button_down(id)
      if id == Gosu::KbEscape
        pause = PauseState.instance
        pause.play_state = self
        GameState.switch(pause)
      end
    end

    protected

    def won?
      @scoreboard.food_count <= 0
    end

    def lost?
      @scoreboard.lives <= 0
    end

    # If game is over, switch to OverState
    def check_game_over
      if lost?
        over_state = OverState.instance
        over_state.play_state = self
        GameState.switch(over_state)
      end
      if won?
        over_state = OverState.instance
        over_state.play_state = self
        over_state.won = true
        GameState.switch(over_state)
      end
    end

    # Returns true if current pacman tile is already occupied by a ghost
    def check_ghost_collision
      @ghosts.each do |ghost|
        next unless ghost.pos == @player.pos
        if ghost.scared?
          ghost.reset
          @scoreboard.score += GHOST_SCORE
        else
          @player.reset
          @scoreboard.lives -= 1
        end
      end
    end

    # Returns true if tile currently occupied by pacman contains pellet or star
    def check_food_collision
      x, y = @player.pos
      if @level.contains_pellet?(x, y)
        @level.eat(x, y)
        @scoreboard.score += PELLET_SCORE
        @scoreboard.food_count -= 1
      elsif @level.contains_star?(x, y)
        @level.eat(x, y)
        @ghosts.each(&:scare)
        @scoreboard.score += PELLET_SCORE
        @scoreboard.food_count -= 1
      end
    end

    def load_ghosts
      @ghosts = []
      @level.ghosts_start.size.times do |idx|
        @ghosts << Ghost.new(self, idx)
      end
    end
  end
end
