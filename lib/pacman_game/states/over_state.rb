require_relative 'game_state'
require 'singleton'

module PacmanGame
  # Final screen when game is over
  class OverState < GameState
    include Singleton

    WIN_MESSAGE = 'Congratulations. You won!'.freeze
    LOSS_MESSAGE = 'Game Over'.freeze

    attr_accessor :won, :play_state

    def initialize
      @won = false
      @message = Gosu::Image.from_text(Window.instance,
                                       @won ? WIN_MESSAGE : LOSS_MESSAGE,
                                       Gosu.default_font_name,
                                       70)
      @info = Gosu::Image.from_text(Window.instance,
                                    "SPACE - Play again\nESCAPE - Quit",
                                    Gosu.default_font_name,
                                    25)
      @play_state = nil
    end

    def update
      @message = Gosu::Image.from_text(Window.instance,
                                       @won ? WIN_MESSAGE : LOSS_MESSAGE,
                                       Gosu.default_font_name,
                                       70)
    end

    def draw
      @message.draw(width_text_offset(@message),
                    height_text_offset(@message),
                    0)
      @info.draw(width_text_offset(@info),
                 height_text_offset(@info) + 100,
                 1)
    end

    def button_down(id)
      Window.instance.close if id == Gosu::KbEscape
      if id == Gosu::KbSpace && @play_state
        level = LevelParser.load_level(@play_state.level.input_file)
        game = PlayState.new(level)
        GameState.switch(game)
      end
    end
  end
end
