module PacmanGame
  # Class representing current state of the game
  class GameState
    # Switch game to another state
    def self.switch(new_state)
      Window.instance.state = new_state
    end

    def width_text_offset(text)
      Window.instance.width / 2 - text.width / 2
    end

    def height_text_offset(text)
      Window.instance.height / 2 - text.height / 2
    end

    def draw
    end

    def update
    end

    def button_down(id)
    end
  end
end
