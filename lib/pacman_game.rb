require_relative '../lib/pacman_game/states/play_state'
require_relative '../lib/pacman_game/level_parser'
require_relative '../lib/pacman_game/window'
require_relative '../lib/pacman_game/direction'

# Main module of pacman game
module PacmanGame
  def self.run(options)
    abort('No specified game level') if options[:level].nil?
    abort('Invalid level file') unless File.file?(options[:level])

    window = Window.instance
    level = LevelParser.load_level(options[:level])
    game = PlayState.new(level)
    window.load_state(game)
    window.show
  end
end
